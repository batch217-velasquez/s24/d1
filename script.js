//console.log("Hello World!");

//[SECTION] Exponent Operator


const firstNum = 8 ** 2;
console.log(firstNum);

//After ES6 Update
const secondNum = Math.pow(8, 2);
console.log(secondNum);

const thirdNum = Math.pow(8, 3);
console.log(thirdNum);

//[SECTION] Template Literals
/*
	- Allows to write strings without using the concatenation operator (+)
	- Greatly helps with code readability
*/

let name = "John";

//pre-template literal strings
let message = 'Hello ' + name + '! Welcome to programming!';
console.log("Message without template literals:" + message);

//Using template literal
//We use backticks (``)
//${variableName}
message = `Hello ${name}! Welcome to programming!`;
console.log(`Message with template literals: ${message}`);

//Multi-line using template literals
const anotherMessage = `
${name} attended a math competition.
He won it by solving the problem 8 ** 2 with solution of ${firstNum}.
`;

console.log(anotherMessage);

const interestRate = .1;
const principal = 1000;

console.log(`The interest on your savings account is: ${principal * interestRate}`);

//[SECTION] Array Destructuring
/*
	- Allows to unpack elements in arrays into distinct variables
	- Allows us to name array elements with variables instead of using index numbers
	- Helps with code readability
	- Syntax
		let/const [variableName, variableName, variableName] = array;
*/

const fullName = ["Juan", "Dela", "Cruz"];

//Pre-Array Destructuring
console.log(fullName[0]); //Juan
console.log(fullName[1]); //Dela
console.log(fullName[2]); //Cruz

console.log(`Hello ${fullName[0]} ${fullName[1]} ${fullName[2]}! It's nice to meet you!`);

//Array Destructuring
const [firstName, middleName, lastName] = fullName;

//Using template literal
console.log(`Hello ${firstName} ${middleName} ${lastName}`);

//Object Destructuring
/*
	- Allows to unpack properties of objects into distinct variables
	- Shortens the syntax for accessing properties from objects
	- Syntax
		let/const {propertyName, propertyName, propertyName} = object;
*/

const person = {
	givenName: "Jane",
	maidenName: "Dela",
	familyName: "Cruz"
};

//Pre-object desstructuring
console.log(person.givenName); //Jane
console.log(person.maidenName); //Dela
console.log(person.familyName); //Cruz

console.log(`Hello  ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you.`);

//Object destructuring
const {givenName, maidenName, familyName} = person;
console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you again.`);

//using destructured variable to a function
function getFullName({givenName, maidenName, familyName}){
	console.log(`${givenName} ${maidenName} ${familyName}`);
}

getFullName(person);

//[SECTION] Arrow Function
/*
	- Compact alternative syntax to traditional functions
	- Useful for code snippets where creating functions will not be reused in any other portion of the code
	- Adheres to the "DRY" (Don't Repeat Yourself) principle where there's no longer need to create a function and think of a name for functions that will only be used in certain code snippets
*/

/*
SYNTAX
const variableName = () => {
	console.log()
}
*/

const hello = () => {
	console.log("Hello World!");
}

hello();

//Traditional Function
/*
Syntax
function functionName(paremeterA, paremeterB, parameterC){
	console.log();
}

*/

/*function printFullName(firstName, middleName, lastName){
	console.log(firstName + " " + middleName + " " + lastName);
}

printFullName("John", "Doe", "Smith");*/

//Arrow Function
const printFullName = (firstName, middleName, lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`);
}

printFullName("John", "Doe", "Smith");

const students = ["John", "Jane", "Judy"];

//Arrow Function with loops
//Pre-arrow Function
students.forEach(function(student){
	console.log(`${student} is a student`);
});

//Arrow function
students.forEach((student) => {
	console.log(`${student} is a student`);
});

//[SECTION] Default function argument value
// Provides a default argument value if none is provided when the function is invoked

const greet = (name = "No user found") => {
	return `Good morning, ${name}`
}

console.log(greet());
console.log(greet("Zedrick"));
console.log(greet());

//[SECTION] Class-based Object blueprints
/*
	- Allows creation/instantiation of objects using classes as blueprints
*/

//Creating a class
/*
Syntax
class className{
	constructor(objectPropertyA, objectProperyB){
		this.objectPropertyA = objectPropertyA;
		this.objectPropertyB = objectPropertyB;
	}
}
*/

class Car {
	constructor(brand, name, year){
		this.brand = brand,
		this.name = name,
		this.year = year
	}
}

const myCar = new Car();
console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);

const myNewCar = new Car("Toyota", "Vios", 2021);
console.log(myNewCar);



